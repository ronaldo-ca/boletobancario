<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!-- the below three lines are a fix to get HTML5 semantic elements working in old versions of Internet Explorer-->
    <!--[if lt IE 9]>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script> --}}

    @yield('resources')
</head>
<body>
    <div id="app">        
        <main class="py-4 py-4 col-10 float-right">
            @yield('content')
        </main>
    </div>
    <footer>
        <script type="text/javascript" src="https://sandbox.boletobancario.com/boletofacil/wro/direct-checkout.min.js"></script>
        <script type="text/javascript">
        
        var publicKey = 'F7485639E088D379C6700ADD5D247D245956006182CB1DB0BCF29BA541DA0089';
        
        var checkout = new DirectCheckout(publicKey, false);
        
        var cardData = {
            cardNumber: '5480452215505109',
            holderName: 'Fulano de Tal',
            securityCode: '555',
            expirationMonth: '12',
            expirationYear: '2018'
        };
        
        var creditCardHash = null;
        
        checkout.getCardHash(cardData, function(cardHash) {
            console.log("Success");
            console.log(cardHash);
            creditCardHash = cardHash;
            test();
        }, function(error) {
            console.log(error);
        });
        
        function test() {
            var data = null;
            console.log('testando hash de cartao de credito:' + creditCardHash);
        }
        </script>
    </footer>
</body>
</html>
