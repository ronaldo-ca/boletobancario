<?php

namespace App\Helpers;

class SPCConfigHelper
{
  private $spc_env;
  private $env_data;

  public function __construct()
  {
    $this->spc_env = config('spc.env');
    $this->env_data = [
      'sandbox' => [
        'login'     => config('spc.sandbox_login'), 
        'password'     => config('spc.sandbox_password'),
        'url'           => config('spc.sandbox_url'),
      ],
      'production' => [
        'login'     => config('spc.production_login'), 
        'password'     => config('spc.production_password'),
        'url'           => config('spc.production_url'),
      ]
    ]; 
  }

  public function getEnvData($data)
  {
    return $this->env_data[$this->spc_env][$data];
  }
  
  public function getURL($urlParam = '', $query_data = [])
  {
    $method_name = debug_backtrace()[3]['function'];
    $query_str = empty($query_data) ?  '' : '?' . http_build_query($query_data);
    $base_url = $this->getEnvData('url');

    $url_dictionary = [
      'postSpc'   => "/remoting/ws/consulta/consultaWebService?wsdl",
      'postScore' => "/remoting/ws/consulta/consultaWebService?wsdl",
      'postPfPj'  => "/remoting/ws/consulta/buscaWebService?wsdl"
    ];

    return $base_url . $url_dictionary[$method_name] . $query_str;
  }

  public function getHeader()
  {
    return [
      // 'Content-type: text/xml',
      // 'Authorization: Basic Mzk4NTM0OjE5MTIyMDE3',
      // 'Accept: text/xml',
    ];
  }

  public function getCredentialsArray()
  {
    return array('login' => $this->getEnvData('login'), 'password' => $this->getEnvData('password'));
  }

}