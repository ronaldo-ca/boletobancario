<?php

namespace App\Helpers;

class BBConfigHelper
{
  public $token;
  private $url;

  public function __construct()
  {
    $this->token = config('boletobancario.token');
    $this->url = config('boletobancario.url'); 
  }

  public function getURL($urlParam = '', $query_data = [])
  {
    $method_name = debug_backtrace()[2]['function'];
    $query_str = empty($query_data) ?  '' : '?' . http_build_query($query_data);
    $base_url = $this->url;

    $url_dictionary = [
      'boleto'            => "/issue-charge",
      'creditCard'        => "/issue-charge",
      'storedCreditCard'  => "/issue-charge"
    ];

    return $base_url . $url_dictionary[$method_name] . $query_str;
  }

  public function getHeader()
  {
    return [
      // 'Content-type: text/xml',
      // 'Authorization: Basic Mzk4NTM0OjE5MTIyMDE3',
      // 'Accept: text/xml',
    ];
  }
}