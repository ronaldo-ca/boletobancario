<?php

namespace App\Helpers;

class FormatHelper
{

  public static function getContent($result)
  {
    if(!FormatHelper::isValidXML($result))
    {
      return $result;
    }
    $xml = simplexml_load_string($result);

    if($xml !== false)
    {
      $array = json_decode(json_encode($xml),true);
      return $array;
    }
    return $result;
  }
  
  public static function isJson($string)
  {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
  }

  public static function isValidXML($xml)
  {
    if(!is_string($xml))
    {
      return false;
    }
    libxml_use_internal_errors(true);
    $doc = simplexml_load_string($xml);
    if ($doc)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  public static function array2xml($array, $xml = false, $global_var = 'result')
  {
    if ($xml === false)
    {
      $xml = new \SimpleXMLElement('<' . $global_var . '/>');
    }
    foreach ($array as $key => $value)
    {
      if (is_array($value))
      {
        if (array_key_exists(0, $value))
        {
          self::array2xml($value[0], $xml->addChild($key));
          continue;
        }
        self::array2xml($value, $xml->addChild($key));
      }
      else
      {
        $xml->addChild($key, $value);
      }
    }
    // return $xml->asXML();
    return str_replace("\n", '', $xml->asXML());
  }

  public static function xml2array($xml_string)
  {
    return json_decode(json_encode((array)simplexml_load_string($xml_string)), 1);
  }

  public static function timestampToBrDate($aux)
  {
    $date = explode('-', explode('T', $aux)[0]);
    return implode('-', [$date[2], $date[1], $date[0]]);
  }
}