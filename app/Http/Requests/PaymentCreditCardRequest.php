<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentCreditCardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'productUuid'               => 'required',
            'payerName'                 => 'required',
            'payerCpfCnpj'              => 'required',
            'payerEmail'                => 'required|email',
            'payerPhone'                => 'required',
            'billingAddressStreet'      => 'required',
            'billingAddressNumber'      => 'required',
            'billingAddressCity'        => 'required',
            'billingAddressState'       => 'required',
            'billingAddressPostcode'    => 'required',
            // 'creditCardHash'            => 'required',
            // 'creditCardNumber'          => 'required',
            // 'creditCardName'            => 'required',
            // 'creditCardBrand'           => 'required',
        ];
    }
}
