<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\Domain;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name'    => 'required|string|max:255',
            'last_name'     => 'required|string|max:255',
            'cpf'           => 'required|size:14|unique:users',
            'email'         => 'required|string|email|max:255|unique:users',
            'password'      => 'required|string|min:6|confirmed',
            'domain'        => 'required|string|in:person,enterprise',

            'responsible.name'      => 'required_if:domain,enterprise',
            'responsible.cpf_cnpj'  => 'required_if:domain,enterprise'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $data['responsible']['cpf_cnpj'] = isset($data['responsible']['cpf_cnpj']) ? $data['responsible']['cpf_cnpj'] : $data['cpf'];
        
        $domain = Domain::create([
            'name'      => $data['domain'] == 'enterprise' ? $data['responsible']['name'] : $data['first_name'] . $data['last_name'],
            'cpf_cnpj'  => $data['responsible']['cpf_cnpj'],
        ]);
        if (!$domain) return false;
        
        return User::create([
            'domain_id'     => $domain->id,
            'cpf'           => $data['cpf'],
            'first_name'    => $data['first_name'],
            'last_name'     => $data['last_name'],
            'phone'         => $data['phone'] ?? null,
            'birth_date'    => isset($data['birth_date']) ? date('Y-m-d', strtotime($data['birth_date'])) : null,
            'email'         => $data['email'],
            'password'      => Hash::make($data['password']),
        ]);
    }

    protected function registered()
    {
        $user->generateToken();
        // return response()->json(['data' => $user->toArray()], 201);
        return redirect('/home')->with('success', 'Usuário criado com sucesso!'); 
    }
}
