<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function credentials(Request $request)
    {
        // $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
        //     ? $this->username()
        //     : 'cpf';
        // return [
        //     $field => $request->get($this->username()),
        //     'password' => $request->password,
        // ];
        return [
            'cpf' => $request->get('email'),
            'password' => $request->password,
        ];
    }

    protected function authenticated(Request $request, $user)
    {
        $request->session()->put('token', $user->generateToken());
    }

    public function logout(Request $request)
    {
        \Auth::user()->update(['api_token' => null]);
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');
    }
}
