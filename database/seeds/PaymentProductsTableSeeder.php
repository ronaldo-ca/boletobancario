<?php

use Illuminate\Database\Seeder;

class PaymentProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('payment_products')->truncate();
        App\Models\PaymentProduct::create([
            'price' => 50
        ]);
        App\Models\PaymentProduct::create([
            'price' => 100
        ]);
        App\Models\PaymentProduct::create([
            'price' => 200
        ]);
        App\Models\PaymentProduct::create([
            'price' => 350
        ]);
    }
}
