<?php

use Illuminate\Database\Seeder;
use App\Models\Domain;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \DB::table('users')->truncate();
        \DB::table('domains')->truncate();

        $domain = Domain::create([
            'name'      => 'RonaldoMoraes',
            'cpf_cnpj'  => '113.926.846-50',
        ]);
        User::create([
            'domain_id'     => $domain->id,
            'cpf'           => '113.926.846-50',
            'first_name'    => 'Ronaldo',
            'last_name'     => 'Moraes',
            'phone'         => '991458401',
            'birth_date'    => date('Y-m-d', strtotime('01/10/1990')),
            'email'         => 'ronaldo.moraes1990@gmail.com',
            'password'      => Hash::make('123123'),
        ])->generateToken();

        $domain2 = Domain::create([
            'name'      => 'Thiago',
            'cpf_cnpj'  => '012.345.678-90',
        ]);
        User::create([
            'domain_id'     => $domain2->id,
            'cpf'           => '012.345.678-90',
            'first_name'    => 'Thiago',
            'last_name'     => 'Locão',
            'phone'         => '999999999',
            'birth_date'    => date('Y-m-d', strtotime('01/10/1990')),
            'email'         => 'thiago.vilasboas@casaamerica.com.br',
            'password'      => Hash::make('123456'),
        ])->generateToken();
    }
}
