<?php

use Illuminate\Database\Seeder;

class PaymentStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('payment_statuses')->truncate();

        App\Models\PaymentStatus::create([
            'name'          => 'AUTHORIZED',
            'description'   => 'Pagamento autorizado (Aguardando confirmação)'
        ]);
        App\Models\PaymentStatus::create([
            'name'          => 'DECLINED',
            'description'   => 'Pagamento rejeitado pela análise de risco'
        ]);
        App\Models\PaymentStatus::create([
            'name'          => 'FAILED',
            'description'   => 'Pagamento não realizado'
        ]);
        App\Models\PaymentStatus::create([
            'name'          => 'NOT_AUTHORIZED',
            'description'   => 'Pagamento não autorizado pela instituição responsável pelo cartão de crédito'
        ]);
        App\Models\PaymentStatus::create([
            'name'          => 'CONFIRMED',
            'description'   => 'Pagamento confirmado'
        ]);
    }
}
