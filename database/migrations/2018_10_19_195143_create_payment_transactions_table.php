<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_transactions', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('uuid')->nullable()->unique();
            
            $table->integer('domain_id')->unsigned();
            $table->foreign('domain_id')->references('id')->on('domains');

            $table->string('code')->nullable()->unique();
            $table->float('amount'); //?
            $table->string('due_date')->nullable();
            $table->string('checkoutUrl')->nullable();
            $table->string('link')->nullable();
            $table->string('pay_number')->nullable();
            $table->string('barcode_number')->nullable(); //?

            $table->string('ptype'); // boleto, credit_card, stored_credit_card???

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_transactions');
    }
}
