<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_balances', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('domain_id')->unsigned();
            $table->foreign('domain_id')->references('id')->on('domains');

            $table->double('credit')->default(0);
            $table->double('debit')->default(0);
            $table->double('current');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_balances');
    }
}
